from datetime import datetime

class ProcessLogger():
	def __init__(self, debug=False):
		self.usages = {}
		self.debug = debug

	def updateUsage(self, newUsage):
		# For each gpu
		for i, gpuUsage in enumerate(newUsage):
			gpuMemoryTotal = gpuUsage["memory.total"]
			if self.debug:
				print(i, gpuUsage["uuid"], gpuUsage["memory.used"], "out of", gpuMemoryTotal)
			uuid = gpuUsage["uuid"]

			if not uuid in self.usages:
				self.usages[uuid] = {}

			thisGpuUsage = self.usages[uuid]
			thisGpuUids = []
			# For each process in this gpu
			for process in gpuUsage["processes"]:
				uid = process["uid"]
				username = process["username"]
				# We cannot log for "root", because this can be anybody. It is recommended nobody uses root for running
				#  GPU processes (other than X server and w/e else the OS uses).
				if username == "root":
					continue

				if not uid in thisGpuUsage:
					thisGpuUsage[uid] = {"memory_usage" : 0, "commands" : [], "count_entries" : 0}

				currentUserUsage = thisGpuUsage[uid]
				thisGpuUids.append(uid)

				thisPercentUsage = process["gpu_memory_usage"] / gpuMemoryTotal
				newUserMemoryUsage = currentUserUsage["memory_usage"] * currentUserUsage["count_entries"] \
					+ thisPercentUsage
				newUserMemoryUsage /= (currentUserUsage["count_entries"] + 1)

				currentUserUsage["commands"].append(process["command"])
				newCommands = list(set(currentUserUsage["commands"]))
				thisGpuUsage[uid] = {"memory_usage" : newUserMemoryUsage, "commands" : newCommands, \
					"count_entries" : currentUserUsage["count_entries"] + 1}

			# For this GPU, also update the users that have not used it this polling but had previous entries
			for uid in thisGpuUsage:
				if uid in thisGpuUids:
					continue
				currentUserUsage = thisGpuUsage[uid]
				newUserMemoryUsage = currentUserUsage["memory_usage"] * currentUserUsage["count_entries"]
				newUserMemoryUsage /= (currentUserUsage["count_entries"] + 1)
				thisGpuUsage[uid]["memory_usage"] = newUserMemoryUsage
				thisGpuUsage[uid]["count_entries"] += 1
			if self.debug:
				print(thisGpuUsage)

	def clearUsage(self):
		if self.debug:
			now = datetime.now()
			print("Cleared usage at %s" % (now))
		self.usages = {}