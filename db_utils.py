import sqlite3
import os
import time
from datetime import datetime

class UsageDB():
	def __init__(self):
		Path = os.path.dirname(os.path.abspath(__file__))
		if not os.path.exists(Path + os.sep + "logs"):
			os.makedirs(Path + os.sep + "logs")
		now = datetime.now()
		dbName = Path + os.sep + "logs/%d-%d-cetal_gpu_usage.db" % (now.year, now.month)
		self.connection = sqlite3.connect(dbName)
		self.cursor = self.connection.cursor()
		self.uuids = {}
		self.checkDb()
		print("Connection to %s established." % (dbName))

	def checkDb(self):
		self.cursor.execute("CREATE TABLE IF NOT EXISTS gpu_ids (uuid text)")
		self.cursor.execute("CREATE TABLE IF NOT EXISTS logs (uid int, gpu_id int, date int, gpu_usage real, processes text)")
		self.connection.commit()

	def updateGpuUsage(self, usages):
		unix_now = int(time.time())
		statements = []
		for gpu_uuid in usages:
			gpu_id = self.getGpuIdFromUuid(gpu_uuid)
			gpu_usage = usages[gpu_uuid]
			for uid in gpu_usage:
				memoryUsage = gpu_usage[uid]["memory_usage"]
				commands = ','.join(map(str, gpu_usage[uid]["commands"]))
				statements.append((uid, gpu_id, unix_now, memoryUsage, commands))
		print(statements)
		self.cursor.executemany("INSERT INTO logs VALUES (?,?,?,?,?)", statements)
		self.connection.commit()

	def getGpuIdFromUuid(self, uuid):
		# get it from memory dict
		if uuid in self.uuids:
			return self.uuids[uuid]

		# otherwise look if it exists in db and update memory dict
		for row in self.cursor.execute("SELECT oid, uuid FROM gpu_ids"):
			row_id, row_uuid = row
			if uuid == row_uuid:
				assert row_id not in self.uuids.values()
				self.uuids[uuid] = row_id
				return row_id

		# If this GPU is not in the gpu_ids table, insert it and return the new added index.
		self.cursor.execute("INSERT INTO gpu_ids VALUES (?)", (uuid,))
		self.connection.commit()
		for row in self.cursor.execute("SELECT oid, uuid FROM gpu_ids"):
			row_id, row_uuid = row
			if uuid == row_uuid:
				assert row_id not in self.uuids.values()
				self.uuids[uuid] = row_id
				return row_id

		assert False, "Impossible to reach here without having an id for this GPU"

	def close(self):
		self.connection.close()