import py3nvml.py3nvml as N
import psutil
import os

def get_process_info(nv_process):
	"""Get the process information of specific pid"""
	process = {}
	ps_process = psutil.Process(pid=nv_process.pid)
	process['username'] = ps_process.username()
	process['uid'] = ps_process.uids()[0]
	# cmdline returns full path;
	# as in `ps -o comm`, get short cmdnames.
	_cmdline = ps_process.cmdline()
	if not _cmdline:
		# sometimes, zombie or unknown (e.g. [kworker/8:2H])
		process['command'] = '?'
	else:
		process['command'] = os.path.basename(_cmdline[0])
	# Bytes to MBytes
	MB = 1024 * 1024
	process['gpu_memory_usage'] = nv_process.usedGpuMemory // MB
	process['pid'] = nv_process.pid
	return process

def get_gpu_info(handle):
	"""Get one GPU information specified by nvml handle"""

	def _decode(b):
		if isinstance(b, bytes):
			return b.decode()    # for python3, to unicode
		return b

	name = _decode(N.nvmlDeviceGetName(handle))
	uuid = _decode(N.nvmlDeviceGetUUID(handle))

	try:
		memory = N.nvmlDeviceGetMemoryInfo(handle)  # in Bytes
	except N.NVMLError:
		memory = None  # Not supported

	try:
		utilization = N.nvmlDeviceGetUtilizationRates(handle)
	except N.NVMLError:
		utilization = None  # Not supported

	try:
		nv_comp_processes = \
			N.nvmlDeviceGetComputeRunningProcesses(handle)
	except N.NVMLError:
		nv_comp_processes = None  # Not supported
	try:
		nv_graphics_processes = \
			N.nvmlDeviceGetGraphicsRunningProcesses(handle)
	except N.NVMLError:
		nv_graphics_processes = None  # Not supported

	if nv_comp_processes is None and nv_graphics_processes is None:
		processes = None
	else:
		processes = []
		nv_comp_processes = nv_comp_processes or []
		nv_graphics_processes = nv_graphics_processes or []
		for nv_process in nv_comp_processes + nv_graphics_processes:
			try:
				process = get_process_info(nv_process)
				processes.append(process)
			except psutil.NoSuchProcess:
				pass

	index = N.nvmlDeviceGetIndex(handle)
	MB = 1024 * 1024
	gpu_info = {
		'index': index,
		'uuid': uuid,
		'name': name,
		'utilization.gpu': utilization.gpu if utilization else None,
		'memory.used': memory.used // MB if memory else None,
		'memory.total': memory.total // MB if memory else None,
		'processes': processes,
	}
	return gpu_info

def getGpuUsage(handles):
	gpus_info = []
	for handle in handles:
		gpu_info = get_gpu_info(handle)
		gpus_info.append(gpu_info)
	return gpus_info