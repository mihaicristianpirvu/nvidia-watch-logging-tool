import time
import os
import py3nvml.py3nvml as N
from datetime import datetime
from argparse import ArgumentParser
import psutil

from gpu_utils import getGpuUsage
from process_logger import ProcessLogger
from db_utils import UsageDB
from export_log import export_log

def getArgs():
	parser = ArgumentParser()
	parser.add_argument("type")
	parser.add_argument("--db_path")

	args = parser.parse_args()
	assert args.type in ("monitor", "export_log")
	if args.type == "export_log":
		assert not args.db_path is None, "When using export_log, a path to the database must be provided"
		args.db_path = os.path.abspath(args.db_path)
	return args

def check_already_running():
	pids = psutil.pids()
	thisPid = os.getpid()
	thisPPid = os.getppid()
	for pid in pids:
		p = psutil.Process(pid)
		cmdline = p.cmdline()
		# Condition for cases where optirun or other tools are used to start GPU manually
		# i.e. optirun python main.py logger creates 2 processes
		if pid == thisPid or pid == thisPPid:
			continue
		if ("python" in cmdline or "python3" in cmdline) and __file__ in cmdline and "monitor" in cmdline:
			return pid
	return None

def monitor():
	N.nvmlInit()
	db = UsageDB()
	logger = ProcessLogger()

	intervalFlush = 5
	dbReset = False
	dbUpdate = False
	while True:
		time.sleep(1)
		now = datetime.now()

		device_count = N.nvmlDeviceGetCount()
		handles = [N.nvmlDeviceGetHandleByIndex(index) for index in range(device_count)]
		gpuUsage = getGpuUsage(handles)
		# print("Num gpus: %d. Info: %s" % (len(gpuUsage), gpuUsage))

		logger.updateUsage(gpuUsage)

		# Reset DB connection every hour
		if now.minute == 0 and now.second < 15 and dbReset == False:
			db.close()
			db = UsageDB()
			dbReset = True

		if now.second >= 15:
			dbReset == False

		if now.second < intervalFlush and dbUpdate == False:
			# Update DB
			db.updateGpuUsage(logger.usages)
			logger.clearUsage()
			dbUpdate = True

		if now.second >= intervalFlush:
			dbUpdate = False

	N.nvmlShutdown()

def main():
	args = getArgs()

	if args.type == "monitor":
		pid = check_already_running()
		if pid != None:
			print("The tool is already running (PID %d)." % (pid))
		else:
			monitor()
	else:
		export_log(args.db_path)

if __name__ == "__main__":
	main()