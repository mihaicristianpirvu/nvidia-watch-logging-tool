import sqlite3
import os
import ntpath
import time
import calendar
from datetime import date, datetime
import numpy as np
from pylatex import Document, LongTable, MultiColumn
from functools import reduce

def path_leaf(path):
	head, tail = ntpath.split(path)
	return tail or ntpath.basename(head)

def toUnix(year, month, day):
	return time.mktime(date(year, month, day).timetuple())

def computeEpochTimes(year, month):
	monthStart = 1
	monthEnd = calendar.monthrange(year, month)[1]
	week2Start, week3Start, week4Start = 8, 15, 22
	return [toUnix(year, month, i) for i in [monthStart, week2Start, week3Start, week4Start, monthEnd]]

def computeIntervalResults(cursor, intervalStart, intervalEnd):
	# First compute the number of seconds between the two timestamps, so we know the number of 60s intervals we work
	#  with, so we can compute the statisti for this time interval
	diff = datetime.fromtimestamp(intervalEnd) - datetime.fromtimestamp(intervalStart)
	numSeconds = diff.total_seconds()
	numIntervals = numSeconds // 60

	results = {}
	cursor.execute("select * from logs where date >= ? and date < ?", (intervalStart, intervalEnd))
	rows = cursor.fetchall()

	# First, add all the entries in this interval, for all users and all gpus
	for row in rows:
		userId, gpuId, _, gpuUsage, _ = row
		if userId not in results:
			results[userId] = {}
		if gpuId not in results[userId]:
			results[userId][gpuId] = 0

		results[userId][gpuId] += gpuUsage

	# Then, divide this cummulative usage, by the number of 60s intervals, so we get the actual interval usage
	for userId in results:
		for gpuId in results[userId]:
			results[userId][gpuId] /= numIntervals

	return results

# A dict of uid => name
def getSystemUserMappings():
	f = open("/etc/passwd")
	lines = f.readlines()
	res = {}
	for line in lines:
		items = line.split(":")
		user, uid = items[0], int(items[2])
		res[uid] = user
	return res

def padZero(x):
	return "0%d" % (x) if x < 10 else "%d" % (x)

def exportToPdf(results, intervals, outPath, numGpus):
	allUsers = list(set(reduce(lambda x, y : x + y, [list(result.keys()) for result in results])))
	userMappings = getSystemUserMappings()
	allUserNames = []
	for user in allUsers:
		allUserNames.append(userMappings[user])

	geometry_options = {
		"includeheadfoot": True
	}
	doc = Document(page_numbers=True, geometry_options=geometry_options)

	table_spec = "p{2cm} p{4cm}" + (" l" * numGpus)
	tableHeader = ["User", "interval"]
	for i in range(numGpus):
		tableHeader.append("GPU%d" % (i + 1))

	with doc.create(LongTable(table_spec)) as data_table:
		data_table.add_hline()
		data_table.add_row(tableHeader)
		data_table.add_hline()
		data_table.end_table_header()
		data_table.add_hline()

		for i, user in enumerate(allUsers):
			userName = allUserNames[i]
			data_table.add_hline()
			data_table.add_row([userName] + [""] * (numGpus + 1))
			totalUsages = np.zeros((numGpus, ))
			for j in range(len(results)):
				if j < len(results) - 1:
					intervalLeft, intervalRight = intervals[j]
					intervalLeft = datetime.fromtimestamp(intervalLeft)
					intervalRight = datetime.fromtimestamp(intervalRight)
					interval = "%s-%s-%s  -  %s-%s-%s" % (intervalLeft.year, intervalLeft.month, \
						padZero(intervalLeft.day), intervalRight.year, intervalRight.month, padZero(intervalRight.day))
				else:
					interval = "Total"
				gpuUsages = [0] * numGpus
				result = results[j]
				if user in result:
					for k in range(1, numGpus + 1):
						if k in result[user]:
							gpuUsages[k - 1] = result[user][k] * 100
				# Convert floats to 2 decimals str and percent
				# print(gpuUsages)
				totalUsages += gpuUsages
				gpuUsages = list(map(lambda x : "%2.2f%%" % (x), gpuUsages))
				intervalLine = ["", interval, *gpuUsages]
				data_table.add_row(intervalLine)

	data_table.end_table_last_footer()
	doc.generate_pdf(outPath, clean_tex=True)

def export_log(dbPath):
	# Initiate connection
	assert dbPath[-3 :] == ".db"
	connection = sqlite3.connect(dbPath)
	cursor = connection.cursor()
	print("Connection to %s established." % (dbPath))

	# Define output PDF path (current directory + db file name)
	dbName = path_leaf(dbPath)
	outPath = dbName[0 : -3]
	# Based on db file name convention, get year/month of this log file, which is used to generate the unix timsetamp
	#  of the month and weeks beggining/end values, further used to compute statistics
	dbName = dbName.split("-")
	year, month = int(dbName[0]), int(dbName[1])
	monthStart, week2Start, week3Start, week4Start, monthEnd = computeEpochTimes(year, month)
	# print(monthStart, week2Start, week3Start, week4Start, monthEnd)

	intervals = [(monthStart, week2Start), (week2Start, week3Start), (week3Start, week4Start), (week4Start, monthEnd)]
	results = []
	for interval in intervals:
		intervalResults = computeIntervalResults(cursor, interval[0], interval[1])
		results.append(intervalResults)
		print(datetime.fromtimestamp(interval[0]), datetime.fromtimestamp(interval[1]))
		print(intervalResults)
		print("_________________________")
	# Add total month usage as well
	intervalResults = computeIntervalResults(cursor, monthStart, monthEnd)
	results.append(intervalResults)
	print(datetime.fromtimestamp(monthStart), datetime.fromtimestamp(monthEnd))
	print(intervalResults)
	print("_________________________")

	print("Exporting to PDF...")
	cursor.execute("select count(uuid) from gpu_ids")
	numGpus = cursor.fetchone()[0]
	exportToPdf(results, intervals, outPath, numGpus)